window.form = $("#form");
window.manage = "ktp/manage";

$(document).ready(function () {
    viewDataTables();
    viewSelect2();
    viewDatePicker();

    $("#table-ktp").on("click", ".table-action", function (event) {
        const data = ($(this).parent().data("value")) ? JSON.parse(decodeURIComponent($(this).parent().data("value"))) : "";
        const clickedValue = $(this).data("value");

        switch ($(this).data("type")) {
            case "kendaraan":
                window.location.href = '/kendaraan/' + data.id;
                break;
            case "edit":
                openForm(data)
                break;
            case "delete":
                removeData(window.manage, data)
                break;
            default:
                break;
        }
    });

    window.validation = form.validate({
        rules: {
            nik: {
                required: true
            },
            nama: {
                required: true
            },
            alamat: {
                required: true
            }
        },
        messages: {
            nik: {
                required: "Form ini harus diisi"
            },
            nama: {
                required: "Form ini harus diisi"
            },
            alamat: {
                required: "Form ini harus diisi"
            }
        }
    });
});

function viewDatePicker() {
    $('#tgl-lahir').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        toggleActive: true
    });
}

function viewSelect2() {
    $("#form").find("select[id=kota]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });

    $("#form").find("select[id=kelamin]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });

    $("#form").find("select[id=golongan]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });

    $("#form").find("select[id=agama]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });

    $("#form").find("select[id=perkawinan]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });

    $("#form").find("select[id=kewarganegaraan]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });
}

function viewDataTables() {
    table = $("#table-ktp").DataTable({
        ajax: {
            url: "ktp/datatable",
            dataType: "json",
            type: "POST",
            data: function (d) {
                d.role = "null";
            }
        },
        processing: false,
        response: true,
        order: [0, 'asc'],
        columns: [{
            data: "id",
            render: function (data, index, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: "nik"
        }, {
            data: "nama"
        }, {
            data: "alamat"
        }, {
            data: "id",
            orderable: false,
            render: function (data, index, row, meta) {

                var button = '<div class="btn-group mr-2" role="group" data-value="' + encodeURIComponent(JSON.stringify(row)) + '">';
                button += '<button type="button" class="table-action btn btn-warning mr-0" data-type="edit" data-toggle="tooltip" data-placement="top" title="Edit">Edit</button >';
                button += '<button type="button" class="table-action btn btn-success mr-0" data-type="kendaraan" data-toggle="tooltip" data-placement="top" title="kendaraan">Kendaraan</button >';
                button += '<button type="button" class="table-action btn btn-danger mr-0" data-type="delete" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button >';
                button += '</div>';
                return button;
            }
        }],
        "createdRow": function (row, data, index) {
            $('td', row).css({
                'vertical-align': 'middle'
            });
            $('td', row).eq(0).css({
                'text-align': 'center'
            });
            $('td', row).eq(1).css({
                'text-align': 'center'
            });
            $('td', row).eq(4).css({
                'text-align': 'center'
            });
        }
    });
}

function openForm(data = null) {
    resetForm('#form-modal');
    console.log(data)

    if (data === null) {
        $("#form-modal").find('.modal-title').text("Tambah Data KTP");
        $("#form-modal").find('input[name=type]').val("insert");
    } else {
        $("#form-modal").find('.modal-title').text("Edit Data KTP");
        $("#form-modal").find("input[name=type]").val("update");
        $("#form-modal").find("input[name=id]").val(data.id);

        $("#form-modal").find("select[name=kota]").val(data.id_kota).trigger('change');
        $("#form-modal").find("input[name=nik]").val(data.nik);
        $("#form-modal").find("input[name=nama]").val(data.nama);
        $("#form-modal").find("input[name=tempat_lahir]").val(data.tempat_lahir);
        $("#form-modal").find("input[name=tgl_lahir]").val(convertMilisecondToDateView(data.tgl_lahir));
        $("#form-modal").find("select[name=kelamin]").val(data.jenis_kelamin).trigger('change');
        $("#form-modal").find("select[name=golongan]").val(data.golongan_darah).trigger('change');
        $("#form-modal").find("textarea[name=alamat]").val(data.alamat);
        $("#form-modal").find("select[name=agama]").val(data.agama).trigger('change');
        $("#form-modal").find("select[name=perkawinan]").val(data.status_perkawinan).trigger('change');
        $("#form-modal").find("input[name=pekerjaan]").val(data.pekerjaan);
        $("#form-modal").find("select[name=kewarganegaraan]").val(data.kewarganegaraan).trigger('change');
    }

    $("#form-modal").modal()
}

function successAction(data) {
    $("#overlay").css("display", "none");
    if ((data.error == 1) || (data.error == 2)) {
        sweetalert(data.error, data.message);
    } else {
        closeForm();
        refreshData();
        sweetalert(data.error, data.message);
    }
}
