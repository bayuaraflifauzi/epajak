window.form = $("#form");
window.manage = "pajak-kendaraan/manage";

$(document).ready(function () {
    viewDataTables();
    viewDatePicker();
    viewSelect2();

    $("#table-pajak-kendaraan").on("click", ".table-action", function (event) {
        const data = ($(this).parent().data("value")) ? JSON.parse(decodeURIComponent($(this).parent().data("value"))) : "";
        const clickedValue = $(this).data("value");

        switch ($(this).data("type")) {
            case "status":
                openModalStatus(data)
                break;
            case "edit":
                openForm(data)
                break;
            case "delete":
                removeData(window.base_url + "/" + window.manage, data)
                break;
            default:
                break;
        }
    });

    window.validation = form.validate({
        rules: {
            bulan_pajak: {
                required: true
            },
            biaya: {
                required: true
            }
        },
        messages: {
            bulan_pajak: {
                required: "Form ini harus diisi"
            },
            biaya: {
                required: "Form ini harus diisi"
            }
        }
    });
});

function viewSelect2() {
    // $("#form").find("select[id=kelompok-aspek]").select2({
    //     theme: "bootstrap",
    //     dropdownParent: $('#form')
    // });
}

function viewDatePicker() {
    $('input[id=bulan-pajak]').datepicker({
        todayHighlight: true,
        autoclose: true,
        format: "yyyy-mm",
        viewMode: "months",
        minViewMode: "months",
        orientation: "bottom"
    }).datepicker('setDate', new Date());
}

function viewDataTables() {
    table = $("#table-pajak-kendaraan").DataTable({
        ajax: {
            url: window.base_url + "/pajak-kendaraan/datatable",
            dataType: "json",
            type: "POST",
            data: function (d) {
                d.id_kendaraan = $("#id-kendaraan-now").val();
            }
        },
        processing: false,
        response: true,
        order: [0, 'asc'],
        columns: [{
            data: "id",
            render: function (data, index, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: "bulan_pajak",
            render: function (data, index, row, meta) {
                return convertMilisecondToDate(data);
            }
        }, {
            data: "biaya",
            render: function (data, index, row, meta) {
                return formatRupiah(data, 'Rp');
            }
        }, {
            data: "id",
            orderable: false,
            render: function (data, index, row, meta) {

                var button = '<div class="btn-group mr-2" role="group" data-value="' + encodeURIComponent(JSON.stringify(row)) + '">';
                button += '<button type="button" class="table-action btn btn-warning mr-0" data-type="edit" data-toggle="tooltip" data-placement="top" title="Edit">Edit</button >';
                button += '<button type="button" class="table-action btn btn-success mr-0" data-type="status" data-toggle="tooltip" data-placement="top" title="Status">Edit Status</button >';
                button += '<button type="button" class="table-action btn btn-danger mr-0" data-type="delete" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button >';
                button += '</div>';
                return button;
            }
        }],
        "createdRow": function (row, data, index) {
            $('td', row).css({
                'vertical-align': 'middle'
            });
            $('td', row).eq(0).css({
                'text-align': 'center'
            });
            $('td', row).eq(3).css({
                'text-align': 'center'
            });
        }
    });
}

function openForm(data = null) {
    resetForm('#form-modal');
    if (data === null) {
        $('input[id=bulan-pajak]').datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "yyyy-mm",
            viewMode: "months",
            minViewMode: "months",
            orientation: "bottom"
        }).datepicker('setDate', new Date());

        $("#form-modal").find('.modal-title').text("Tambah Data Pajak Kendaraan");
        $("#form-modal").find('input[name=type]').val("insert");
    } else {
        $("#form-modal").find('.modal-title').text("Edit Data Pajak Kendaraan");
        $("#form-modal").find("input[name=type]").val("update");
        $("#form-modal").find("input[name=id]").val(data.id);

        $("#form-modal").find("input[name=bulan_pajak]").val(convertMilisecondToDate(data.bulan_pajak));
        $("#form-modal").find("input[name=biaya]").val(data.biaya);
    }

    $("#form-modal").modal()
}

function openModalStatus(data = null) {
    resetForm('#modal-status');
    $("#modal-status").find("input[name=id]").val(data.id);
    $("#modal-status").find("input[name=bulan]").val(convertMilisecondToDate(data.bulan_pajak));
    $("#modal-status").find("input[name=biaya]").val(data.biaya);

    if (data.status == 2) {
        $("#modal-status").find("#button-batal").prop("hidden", false);
        $("#modal-status").find("#button-terima").prop("hidden", false);
    } else {
        $("#modal-status").find("#button-batal").prop("hidden", true);
        $("#modal-status").find("#button-terima").prop("hidden", true);
    }

    $("#modal-status").modal();
}

function changeStatus(status) { 
    $.ajax({
         url: window.base_url + "/" + window.manage,
         type: "POST",
         data: {
             id: $("#modal-status").find("input[name=id]").val(),
             status: status, 
             type: 'changeStatus'
         },
         dataType: "JSON",
         beforeSend: function () {
             $("#overlay").css("display", "block");
         },
         success: function (row) {
             $("#overlay").css("display", "none");
             sweetalert(row.error, row.message);
             $("#modal-status").modal("hide");
             refreshData();
         }
     });
}

function successAction(data) {
    $("#overlay").css("display", "none");
    if ((data.error == 1) || (data.error == 2)) {
        sweetalert(data.error, data.message);
    } else {
        closeForm();
        refreshData();
        sweetalert(data.error, data.message);
    }
}
