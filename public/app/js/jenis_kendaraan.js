window.form = $("#form");
window.manage = "jenis-kendaraan/manage";

$(document).ready(function () {
    viewDataTables();
    viewSelect2();

    $("#table-jenis-kendaraan").on("click", ".table-action", function (event) {
        const data = ($(this).parent().data("value")) ? JSON.parse(decodeURIComponent($(this).parent().data("value"))) : "";
        const clickedValue = $(this).data("value");

        switch ($(this).data("type")) {
            case "detail":
                detail(data)
                break;
            case "edit":
                openForm(data)
                break;
            case "delete":
                removeData(window.manage, data)
                break;
            default:
                break;
        }
    });

    window.validation = form.validate({
        rules: {
            nama: {
                required: true
            }
        },
        messages: {
            nama: {
                required: "Form ini harus diisi"
            }
        }
    });
});

function viewSelect2() {
    // $("#form").find("select[id=kelompok-aspek]").select2({
    //     theme: "bootstrap",
    //     dropdownParent: $('#form')
    // });
}

function viewDataTables() {
    table = $("#table-jenis-kendaraan").DataTable({
        ajax: {
            url: "jenis-kendaraan/datatable",
            dataType: "json",
            type: "POST",
            data: function (d) {
                d.role = "null";
            }
        },
        processing: false,
        response: true,
        order: [0, 'asc'],
        columns: [{
            data: "id",
            render: function (data, index, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: "nama"
        }, {
            data: "id",
            orderable: false,
            render: function (data, index, row, meta) {

                var button = '<div class="btn-group mr-2" role="group" data-value="' + encodeURIComponent(JSON.stringify(row)) + '">';
                button += '<button type="button" class="table-action btn btn-warning mr-0" data-type="edit" data-toggle="tooltip" data-placement="top" title="Edit">Edit</button >';
                button += '<button type="button" class="table-action btn btn-danger mr-0" data-type="delete" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button >';
                button += '</div>';
                return button;
            }
        }],
        "createdRow": function (row, data, index) {
            $('td', row).css({
                'vertical-align': 'middle'
            });
            $('td', row).eq(0).css({
                'text-align': 'center'
            });
            $('td', row).eq(2).css({
                'text-align': 'center'
            });
        }
    });
}

function openForm(data = null) {
    resetForm('#form-modal');
    if (data === null) {
        $("#form-modal").find('.modal-title').text("Tambah Data Jenis Kendaraan");
        $("#form-modal").find('input[name=type]').val("insert");
    } else {
        $("#form-modal").find('.modal-title').text("Edit Data Jenis Kendaraan");
        $("#form-modal").find("input[name=type]").val("update");
        $("#form-modal").find("input[name=id]").val(data.id);

        $("#form-modal").find("input[name=nama]").val(data.nama);
    }

    $("#form-modal").modal()
}

function successAction(data) {
    $("#overlay").css("display", "none");
    if ((data.error == 1) || (data.error == 2)) {
        sweetalert(data.error, data.message);
    } else {
        closeForm();
        refreshData();
        sweetalert(data.error, data.message);
    }
}
