window.form = $("#form");
window.manage = "ktp/manage";

$(document).ready(function () {
    window.validation = form.validate({
        rules: {
            ktp: {
                required: true
            }
        },
        messages: {
            ktp: {
                required: "Form ini harus diisi"
            }
        }
    });
});

function successAction(data) {
    $("#overlay").css("display", "none");
    if ((data.error == 1) || (data.error == 2)) {
        sweetalert(data.error, data.message);
    } else {
        let timerInterval
        Swal.fire({
          icon: 'success',
          title: 'NIK KTP ANDA TELAH DITEMUKAN!',
          html: 'APLIKASI SEDANG MEMPROSES DALAM <b></b> MILIDETIK.',
          timer: 5000,
          timerProgressBar: true,
          onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
              const content = Swal.getContent()
              if (content) {
                const b = content.querySelector('b')
                if (b) {
                  b.textContent = Swal.getTimerLeft()
                }
              }
            }, 100)
          },
          onClose: () => {
            clearInterval(timerInterval)
          }
        }).then((result) => {
          /* Read more about handling dismissals below */
          if (result.dismiss === Swal.DismissReason.timer) {
            window.location.href = window.base_url + '/transaksi/' + data.ktp[0].id;
          }
        })
    }
}
