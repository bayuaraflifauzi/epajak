window.form = $("#form");
window.manage = "kendaraan/manage";

$(document).ready(function () {
    viewDataTables();
    viewSelect2();

    $("#table-kendaraan").on("click", ".table-action", function (event) {
        const data = ($(this).parent().data("value")) ? JSON.parse(decodeURIComponent($(this).parent().data("value"))) : "";
        const clickedValue = $(this).data("value");

        switch ($(this).data("type")) {
            case "pajak":
                window.location.href = window.base_url + '/pajak-kendaraan/' + data.id;
                break;
            case "edit":
                openForm(data)
                break;
            case "delete":
                removeData(window.base_url + "/" + window.manage, data)
                break;
            default:
                break;
        }
    });

    window.validation = form.validate({
        rules: {
            merk: {
                required: true
            },
            tahun_kendaraan: {
                required: true
            },
            model: {
                required: true
            },
            warna: {
                required: true
            },
            no_rangka: {
                required: true
            },
            no_mesin: {
                required: true
            },
            no_polisi: {
                required: true
            }
        },
        messages: {
            merk: {
                required: "Form ini harus diisi"
            },
            tahun_kendaraan: {
                required: "Form ini harus diisi"
            },
            model: {
                required: "Form ini harus diisi"
            },
            warna: {
                required: "Form ini harus diisi"
            },
            no_rangka: {
                required: "Form ini harus diisi"
            },
            no_mesin: {
                required: "Form ini harus diisi"
            },
            no_polisi: {
                required: "Form ini harus diisi"
            }
        }
    });
});

function viewSelect2() {
    // $("#form").find("select[id=kelompok-aspek]").select2({
    //     theme: "bootstrap",
    //     dropdownParent: $('#form')
    // });
}

function viewDataTables() {
    table = $("#table-kendaraan").DataTable({
        ajax: {
            url: window.base_url + "/kendaraan/datatable",
            dataType: "json",
            type: "POST",
            data: function (d) {
                d.id_ktp = $("#id-ktp-now").val();
            }
        },
        processing: false,
        response: true,
        order: [0, 'asc'],
        columns: [{
            data: "id",
            render: function (data, index, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: "merk"
        }, {
            data: "no_polisi"
        }, {
            data: "no_rangka"
        }, {
            data: "no_mesin"
        }, {
            data: "id",
            orderable: false,
            render: function (data, index, row, meta) {

                var button = '<div class="btn-group mr-2" role="group" data-value="' + encodeURIComponent(JSON.stringify(row)) + '">';
                button += '<button type="button" class="table-action btn btn-warning mr-0" data-type="edit" data-toggle="tooltip" data-placement="top" title="Edit">Edit</button >';
                button += '<button type="button" class="table-action btn btn-success mr-0" data-type="pajak" data-toggle="tooltip" data-placement="top" title="Data Pajak">Data Pajak</button >';
                button += '<button type="button" class="table-action btn btn-danger mr-0" data-type="delete" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button >';
                button += '</div>';
                return button;
            }
        }],
        "createdRow": function (row, data, index) {
            $('td', row).css({
                'vertical-align': 'middle'
            });
            $('td', row).eq(0).css({
                'text-align': 'center'
            });
            $('td', row).eq(5).css({
                'text-align': 'center'
            });
        }
    });
}

function openForm(data = null) {
    resetForm('#form-modal');
    if (data === null) {
        $("#form-modal").find('.modal-title').text("Tambah Data Kendaraan");
        $("#form-modal").find('input[name=type]').val("insert");
    } else {
        $("#form-modal").find('.modal-title').text("Edit Data Kendaraan");
        $("#form-modal").find("input[name=type]").val("update");
        $("#form-modal").find("input[name=id]").val(data.id);

        $("#form-modal").find("select[name=id_jenis_kendaraan]").val(data.id_jenis_kendaraan);
        $("#form-modal").find("input[name=merk]").val(data.merk);
        $("#form-modal").find("input[name=tahun_kendaraan]").val(data.tahun_kendaraan);
        $("#form-modal").find("input[name=model]").val(data.model);
        $("#form-modal").find("input[name=warna]").val(data.warna);
        $("#form-modal").find("input[name=no_rangka]").val(data.no_rangka);
        $("#form-modal").find("input[name=no_mesin]").val(data.no_mesin);
        $("#form-modal").find("input[name=no_polisi]").val(data.no_polisi);
        $("#form-modal").find("input[name=id_ktp]").val(data.id_ktp);
    }

    $("#form-modal").modal()
}

function successAction(data) {
    $("#overlay").css("display", "none");
    if ((data.error == 1) || (data.error == 2)) {
        sweetalert(data.error, data.message);
    } else {
        closeForm();
        refreshData();
        sweetalert(data.error, data.message);
    }
}
