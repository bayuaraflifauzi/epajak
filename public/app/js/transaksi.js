window.form = $("#form");
window.manage = "/transaksi/manage";
window.totalPajak = 0;
window.id_histori_kendaraan = null;

$(document).ready(function() {
    viewDataTablesBayarPajak();
    viewKTP();
    viewDataTablesKendaraan();
    viewDataTablesHirtori();
    viewDataTablesHirtoriKendaraan();
    viewSelect2();

    $("#table-bayar-pajak").on("click", ".table-action", function(event) {
        const data = ($(this).parent().data("value")) ? JSON.parse(decodeURIComponent($(this).parent().data("value"))) : "";
        const clickedValue = $(this).data("value");

        switch ($(this).data("type")) {
            case "detail":
                openForm(data)
                break;
            default:
                break;
        }
    });

    $("#table-kendaraan-wb").on("click", ".table-action", function(event) {
        const data = ($(this).parent().data("value")) ? JSON.parse(decodeURIComponent($(this).parent().data("value"))) : "";
        const clickedValue = $(this).data("value");

        switch ($(this).data("type")) {
            case "detail":
                openFormKendaraan(data)
                break;
            default:
                break;
        }
    });

    $("#table-histori").on("click", ".table-action", function(event) {
        const data = ($(this).parent().data("value")) ? JSON.parse(decodeURIComponent($(this).parent().data("value"))) : "";
        const clickedValue = $(this).data("value");

        switch ($(this).data("type")) {
            case "detail":
                window.id_histori_kendaraan = data.id;
                window.tableHistoriKendaraan.ajax.reload();
                $("#table-modal").find('.label-table').text("Detail Transaksi Tanggal " + convertMilisecondToDate(data.tanggal));
                $("#table-modal").modal({
                    backdrop: 'static',
                    keyboard: false
                })
                break;
            default:
                break;
        }
    });

    window.validation = form.validate({
        rules: {
            nama: {
                required: true
            }
        },
        messages: {
            nama: {
                required: "Form ini harus diisi"
            }
        }
    });
});

function viewSelect2() {
    $("#form").find("select[id=kota]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });

    $("#form").find("select[id=kelamin]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });

    $("#form").find("select[id=golongan]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });

    $("#form").find("select[id=agama]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });

    $("#form").find("select[id=perkawinan]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });

    $("#form").find("select[id=kewarganegaraan]").select2({
        theme: "bootstrap",
        dropdownParent: $('#form')
    });
}

function viewDataTablesBayarPajak() {
    table = $("#table-bayar-pajak").DataTable({
        ajax: {
            url: window.base_url + "/transaksi/bayar-pajak/datatable",
            dataType: "json",
            type: "POST",
            data: function(d) {
                d.id_ktp = $('#id-ktp-form').val();
            }
        },
        processing: false,
        response: true,
        order: [0, 'asc'],
        columns: [{
            data: "id",
            render: function(data, index, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: "bulan_pajak",
            render: function(data, index, row, meta) {
                return convertMilisecondToDate(data);
            }
        }, {
            data: "kendaraan.merk"
        }, {
            data: "kendaraan.model"
        }, {
            data: "kendaraan.no_polisi"
        }, {
            data: "biaya",
            render: function(data, index, row, meta) {
                return formatRupiah(data, 'Rp');
            }
        }, {
            data: "id",
            orderable: false,
            render: function(data, index, row, meta) {

                var button = '<div class="btn-group mr-2" role="group" data-value="' + encodeURIComponent(JSON.stringify(row)) + '">';
                button += '<button type="button" class="table-action btn btn-warning mr-0" data-type="detail" data-toggle="tooltip" data-placement="top" title="Detail">Detail Kendaraan</button >';
                button += '</div>';
                return button;
            }
        }, {
            data: "id",
            orderable: false,
            "render": function(data, type, full, meta) {
                return '<input name="IDs[]" value="' + full.id + '" type="checkbox" class="checkRow" onclick="calculate(event, ' + full.biaya + ')">';
            }
        }],
        "createdRow": function(row, data, index) {
            $('td', row).css({
                'vertical-align': 'middle'
            });
            $('td', row).eq(0).css({
                'text-align': 'center'
            });
            $('td', row).eq(6).css({
                'text-align': 'center'
            });
            $('td', row).eq(7).css({
                'text-align': 'center'
            });
        }
    });
}

function viewKTP() {
    $.ajax({
        url: window.base_url + window.manage,
        type: "POST",
        data: {
            type: 'data-ktp',
            id_ktp: $('#id-ktp-form').val()
        },
        dataType: "JSON",
        beforeSend: function() {
            $("#overlay").css("display", "block");
        },
        success: function(data) {
            $("#overlay").css("display", "none");
            $("#form-ktp").find("select[name=kota]").val(data.data.id_kota).trigger('change');
            $("#form-ktp").find("input[name=nik]").val(data.data.nik);
            $("#form-ktp").find("input[name=nama]").val(data.data.nama);
            $("#form-ktp").find("input[name=tempat_lahir]").val(data.data.tempat_lahir);
            $("#form-ktp").find("input[name=tgl_lahir]").val(convertMilisecondToDateView(data.data.tgl_lahir));
            $("#form-ktp").find("select[name=kelamin]").val(data.data.jenis_kelamin).trigger('change');
            $("#form-ktp").find("select[name=golongan]").val(data.data.golongan_darah).trigger('change');
            $("#form-ktp").find("textarea[name=alamat]").val(data.data.alamat);
            $("#form-ktp").find("select[name=agama]").val(data.data.agama).trigger('change');
            $("#form-ktp").find("select[name=perkawinan]").val(data.data.status_perkawinan).trigger('change');
            $("#form-ktp").find("input[name=pekerjaan]").val(data.data.pekerjaan);
            $("#form-ktp").find("select[name=kewarganegaraan]").val(data.data.kewarganegaraan).trigger('change');

        }
    });
}

function viewDataTablesKendaraan() {
    tableKendaraan = $("#table-kendaraan-wb").DataTable({
        ajax: {
            url: window.base_url + "/kendaraan/datatable",
            dataType: "json",
            type: "POST",
            data: function (d) {
                d.id_ktp = $('#id-ktp-form').val();
            }
        },
        processing: false,
        response: true,
        order: [0, 'asc'],
        columns: [{
            data: "id",
            render: function (data, index, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: "merk"
        }, {
            data: "no_polisi"
        }, {
            data: "no_rangka"
        }, {
            data: "no_mesin"
        }, {
            data: "id",
            orderable: false,
            render: function (data, index, row, meta) {

                var button = '<div class="btn-group mr-2" role="group" data-value="' + encodeURIComponent(JSON.stringify(row)) + '">';
                button += '<button type="button" class="table-action btn btn-warning mr-0" data-type="detail" data-toggle="tooltip" data-placement="top" title="Detail">Detail Kendaraan</button >';
                button += '</div>';
                return button;
            }
        }],
        "createdRow": function (row, data, index) {
            $('td', row).css({
                'vertical-align': 'middle'
            });
            $('td', row).eq(0).css({
                'text-align': 'center'
            });
            $('td', row).eq(5).css({
                'text-align': 'center'
            });
        }
    });
}

function viewDataTablesHirtori() {
    window.tableHistori = $("#table-histori").DataTable({
        ajax: {
            url: window.base_url + "/transaksi/histori/datatable",
            dataType: "json",
            type: "POST",
            data: function(d) {
                d.id_ktp = $('#id-ktp-form').val();
            }
        },
        processing: false,
        response: true,
        order: [0, 'asc'],
        columns: [{
            data: "id",
            render: function(data, index, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: "tanggal",
            render: function(data, index, row, meta) {
                return convertMilisecondToDate(data);
            }
        }, {
            data: "no_va"
        }, {
            data: "total",
            render: function(data, index, row, meta) {
                return formatRupiah(data, 'Rp');
            }
        }, {
            data: "status",
            render: function(data, index, row, meta) {
                switch(data){
                    case 0:
                        var button = '<span class="badge badge-warning">PROSES BAYAR</span>'; 
                        break;
                    case 1:
                        var button = '<span class="badge badge-success">SUDAH BAYAR</span>'; 
                        break;
                    case 2:
                        var button = '<span class="badge badge-danger">TOLAK PEMBAYARAN</span>'; 
                        break;
                    default:
                        break;
                }
                return button;

            }
        }, {
            data: "id",
            orderable: false,
            render: function(data, index, row, meta) {

                var button = '<div class="btn-group mr-2" role="group" data-value="' + encodeURIComponent(JSON.stringify(row)) + '">';
                button += '<button type="button" class="table-action btn btn-warning mr-0" data-type="detail" data-toggle="tooltip" data-placement="top" title="Detail">Detail Transaksi</button >';
                button += '</div>';
                return button;
            }
        }],
        "createdRow": function(row, data, index) {
            $('td', row).css({
                'vertical-align': 'middle'
            });
            $('td', row).eq(0).css({
                'text-align': 'center'
            });
            $('td', row).eq(4).css({
                'text-align': 'center'
            });
            $('td', row).eq(5).css({
                'text-align': 'center'
            });
        }
    });
}

function viewDataTablesHirtoriKendaraan() {
   window.tableHistoriKendaraan = $("#table-histori-kendaraan").DataTable({
        ajax: {
            url: window.base_url + "/transaksi/histori-kendaraan/datatable",
            dataType: "json",
            type: "POST",
            data: function(d) {
                d.id_histori = window.id_histori_kendaraan;
            }
        },
        processing: false,
        response: true,
        order: [0, 'asc'],
        columns: [{
            data: "id",
            render: function(data, index, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: "bulan_pajak",
            render: function(data, index, row, meta) {
                return convertMilisecondToDate(data);
            }
        }, {
            data: "kendaraan.merk"
        }, {
            data: "kendaraan.model"
        }, {
            data: "kendaraan.no_polisi"
        }, {
            data: "biaya",
            render: function(data, index, row, meta) {
                return formatRupiah(data, 'Rp');
            }
        }],
        "createdRow": function(row, data, index) {
            $('td', row).css({
                'vertical-align': 'middle'
            });
            $('td', row).eq(0).css({
                'text-align': 'center'
            });
        }
    });
}

function calculate(event, biaya) {
    if ($(event.target).is(':checked')) {
        window.totalPajak = window.totalPajak + biaya;
    } else {
        window.totalPajak = window.totalPajak - biaya;
    }
}

function reviewSelected() {
    if (window.totalPajak == 0) {
        sweetalert(1, "Pilih terlebih dahulu kendaraan anda yang akan dibayar pajak");
    } else {
        $("#form-total-pajak").find('h5[id=label-judul]').text("TOTAL BIAYA BAYAR PAJAK KENDARAAN YANG DIPILIH");
        $("#form-total-pajak").find('h1[id=label-total]').text(formatRupiah(window.totalPajak, 'Rp'));
        $("#form-total-pajak").modal({
            backdrop: 'static',
            keyboard: false
        })
    }
}

function generate() {
    var dataArrayTable = table.$("input[name='IDs[]']").serializeArray(),
        dataArray = [];

    $(dataArrayTable).each(function(i, field) {
        dataArray.push(field.value);
    })

    $.ajax({
        url: window.base_url + "/transaksi/bayar-pajak",
        type: "POST",
        data: {
            list_id_kendaraan: dataArray,
            total: window.totalPajak,
            id_ktp: $('#id-ktp-form').val()
        },
        dataType: "JSON",
        beforeSend: function() {
            $("#overlay").css("display", "block");
        },
        success: function(data) {
            $("#overlay").css("display", "none");
            $('#form-total-pajak').modal('hide');
            refreshData();
            window.totalPajak = 0;

            let timerInterval
            Swal.fire({
                icon: 'success',
                title: 'GENERATE VIRTUAL ACCOUNT SELESAI!',
                html: 'APLIKASI SEDANG MEMPROSES DAN AKAN MENAMPILKAN NO VA DALAM <b></b> MILIDETIK.',
                timer: 5000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    window.tableHistori.ajax.reload()
                    $("#form-va").find('h5[id=label-judul-va]').text("VIRTUAL ACCOUNT");
                    $("#form-va").find('h1[id=label-va]').text(data.va);
                    $("#form-va").modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                }
            })
        }
    });
}

function openForm(data = null) {
    resetForm('#form-modal');
    if (data === null) {
        $("#form-modal").find('.modal-title').text("Informasi Data Pajak Kendaraan");
        $("#form-modal").find('input[name=type]').val("insert");
    } else {
        $("#form-modal").find('.modal-title').text("Informasi Data Pajak Kendaraan");
        $("#form-modal").find("input[name=type]").val("update");
        $("#form-modal").find("input[name=id]").val(data.id);

        $("#form-modal").find("select[name=id_jenis_kendaraan]").val(data.kendaraan.id_jenis_kendaraan);
        $("#form-modal").find("input[name=merk]").val(data.kendaraan.merk);
        $("#form-modal").find("input[name=tahun_kendaraan]").val(data.kendaraan.tahun_kendaraan);
        $("#form-modal").find("input[name=model]").val(data.kendaraan.model);
        $("#form-modal").find("input[name=warna]").val(data.kendaraan.warna);
        $("#form-modal").find("input[name=no_rangka]").val(data.kendaraan.no_rangka);
        $("#form-modal").find("input[name=no_mesin]").val(data.kendaraan.no_mesin);
        $("#form-modal").find("input[name=no_polisi]").val(data.kendaraan.no_polisi);
        $("#form-modal").find("input[name=id_ktp]").val(data.kendaraan.id_ktp);

        $("#form-modal").find("input[name=bulan_pajak]").val(convertMilisecondToDate(data.bulan_pajak));
        $("#form-modal").find("input[name=biaya]").val(formatRupiah(data.biaya, 'Rp'));
    }

    $("#form-modal").modal()
}

function openFormKendaraan(data = null) {
    resetForm('#form-modal-kendaraan');
    if (data === null) {
        $("#form-modal-kendaraan").find('.modal-title').text("Informasi Data Pajak Kendaraan");
        $("#form-modal-kendaraan").find('input[name=type]').val("insert");
    } else {
        $("#form-modal-kendaraan").find('.modal-title').text("Informasi Kendaraan Wajib Pajak");

        $("#form-modal-kendaraan").find("select[name=id_jenis_kendaraan]").val(data.id_jenis_kendaraan);
        $("#form-modal-kendaraan").find("input[name=merk]").val(data.merk);
        $("#form-modal-kendaraan").find("input[name=tahun_kendaraan]").val(data.tahun_kendaraan);
        $("#form-modal-kendaraan").find("input[name=model]").val(data.model);
        $("#form-modal-kendaraan").find("input[name=warna]").val(data.warna);
        $("#form-modal-kendaraan").find("input[name=no_rangka]").val(data.no_rangka);
        $("#form-modal-kendaraan").find("input[name=no_mesin]").val(data.no_mesin);
        $("#form-modal-kendaraan").find("input[name=no_polisi]").val(data.no_polisi);
    }

    $("#form-modal-kendaraan").modal()
}

function successAction(data) {
    $("#overlay").css("display", "none");
    if ((data.error == 1) || (data.error == 2)) {
        sweetalert(data.error, data.message);
    } else {
        closeForm();
        refreshData();
        sweetalert(data.error, data.message);
    }
}