<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$data = array(
        'title' => 'HALAMAN UTAMA',
        'special_css' => '',
        'special_js' => 'welcome.js'
    );

    return view('welcome', $data);
});

Route::get('ktp', 'KTPController@index')->name('ktp.index');
Route::post('ktp/manage', 'KTPController@manage')->name('ktp.manage');
Route::post('ktp/datatable', 'KTPController@datatable')->name('ktp.datatable');

Route::get('jenis-kendaraan', 'JenisKendaraanController@index')->name('jenisKendaraan.index');
Route::post('jenis-kendaraan/manage', 'JenisKendaraanController@manage')->name('jenisKendaraan.manage');
Route::post('jenis-kendaraan/datatable', 'JenisKendaraanController@datatable')->name('jenisKendaraan.datatable');

Route::get('kendaraan', 'KendaraanController@index')->name('kendaraan.index');
Route::post('kendaraan/manage', 'KendaraanController@manage')->name('kendaraan.manage');
Route::post('kendaraan/datatable', 'KendaraanController@datatable')->name('kendaraan.datatable');
Route::get('kendaraan/{id}', 'KendaraanController@getAllKendaraanByKTP')->name('kendaraan.getAllKendaraanByKTP');

Route::get('pajak-kendaraan', 'PajakKendaraanController@index')->name('pajakKendaraan.index');
Route::post('pajak-kendaraan/manage', 'PajakKendaraanController@manage')->name('pajakKendaraan.manage');
Route::post('pajak-kendaraan/datatable', 'PajakKendaraanController@datatable')->name('pajakKendaraan.datatable');
Route::get('pajak-kendaraan/{id}', 'PajakKendaraanController@getAllPajakKendaraanByKendaraan')->name('pajakKendaraan.getAllPajakKendaraanByKendaraan');

Route::get('transaksi/{id}', 'TransactionController@index')->name('transaksi.index');
Route::post('transaksi/manage', 'TransactionController@manage')->name('transaksi.manage');
Route::post('transaksi/bayar-pajak/datatable', 'TransactionController@bayarPajakDatatable')->name('transaksi.bayarPajakDatatable');
Route::post('transaksi/histori/datatable', 'TransactionController@historiDatatable')->name('transaksi.historiDatatable');
Route::post('transaksi/histori-kendaraan/datatable', 'TransactionController@historiKendaraanDatatable')->name('transaksi.historiKendaraanDatatable');
Route::post('transaksi/bayar-pajak', 'TransactionController@generateVirtualAccount')->name('transaksi.generateVirtualAccount');