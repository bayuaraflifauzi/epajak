<?php

namespace App\Http\Controllers;

use App\KTP;
use App\Kendaraan;
use App\JenisKendaraan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KendaraanController extends Controller
{
    public function index()
    {
    	$jenis_kendaraan = JenisKendaraan::where(['deleted_at' => null])->orderBy('created_at', 'DESC')->get();

        $data = array(
            'title' => 'MANAGE KENDARAAN',
            'id_ktp' => '',
            'jenis_kendaraan' => $jenis_kendaraan,
            'special_css' => '',
            'special_js' => 'kendaraan.js'
        );

        return view('kendaraan', $data);
    }

    public function manage(Request $request)
    {
        switch ($request->type) {
            case 'insert':
                $crud = DB::table('kendaraan')->insert([
                    'id' => time(),
                    'merk' => strtoupper($request->merk),
                    'tahun_kendaraan' => $request->tahun_kendaraan,
                    'model' => strtoupper($request->model),
                    'warna' => strtoupper($request->warna),
                    'no_rangka' => strtoupper($request->no_rangka),
                    'no_mesin' => strtoupper($request->no_mesin),
                    'no_polisi' => strtoupper($request->no_polisi),
                    'id_ktp' => $request->id_ktp,
                    'id_jenis_kendaraan' => $request->id_jenis_kendaraan,
                    'created_at' => time()
                ]);
                break;
            case 'update':
                $crud = DB::table('kendaraan')->where('id', $request->id)->update([
                    'merk' => strtoupper($request->merk),
                    'tahun_kendaraan' => $request->tahun_kendaraan,
                    'model' => strtoupper($request->model),
                    'warna' => strtoupper($request->warna),
                    'no_rangka' => strtoupper($request->no_rangka),
                    'no_mesin' => strtoupper($request->no_mesin),
                    'no_polisi' => strtoupper($request->no_polisi),
                    'id_ktp' => $request->id_ktp,
                    'id_jenis_kendaraan' => $request->id_jenis_kendaraan,
                    'updated_at' => time()
                ]);
                break;
            case 'delete':
                $crud = DB::table('kendaraan')->where('id', $request->id)->update([
                    'deleted_at' => time()
                ]);
                break;
            default:
                break;
        }

        if (!$crud)
            return response()->json(['error' => 1, 'message' => "Data Gagal dieksekusi"], 400);

        return response()->json(['error' => 0, 'message' => "Data Berhasil di" . $request->type], 200);
    }

    public function datatable(Request $request)
    {
        $kendaraan = Kendaraan::where(['id_ktp' => $request->id_ktp, 'deleted_at' => null])->orderBy('created_at', 'DESC')->get();
        return response()->json(['data' => $kendaraan], 200);
    }

    public function getAllKendaraanByKTP($id_ktp){

    	$ktp = KTP::find($id_ktp);
    	$jenis_kendaraan = JenisKendaraan::where(['deleted_at' => null])->orderBy('created_at', 'DESC')->get();

    	$data = array(
            'title' => 'MANAGE KENDARAAN KEPEMILIKAN',
            'subTitle' => 'KTP : ' . $ktp->nik,
            'id_ktp' => $id_ktp,
            'jenis_kendaraan' => $jenis_kendaraan,
            'special_css' => '',
            'special_js' => 'kendaraan.js'
        );

        return view('kendaraan', $data);
    }
}
