<?php

namespace App\Http\Controllers;

use App\JenisKendaraan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JenisKendaraanController extends Controller
{
    public function index()
    {
        $data = array(
            'title' => 'MANAGE JENIS KENDARAAN',
            'special_css' => '',
            'special_js' => 'jenis_kendaraan.js'
        );

        return view('jenis_kendaraan', $data);
    }

    public function manage(Request $request)
    {
        switch ($request->type) {
            case 'insert':
                $crud = DB::table('jenis_kendaraan')->insert([
                    'id' => time(),
                    'nama' => ucwords($request->nama),
                    'created_at' => time()
                ]);
                break;
            case 'update':
                $crud = DB::table('jenis_kendaraan')->where('id', $request->id)->update([
                    'nama' => ucwords($request->nama),
                    'updated_at' => time()
                ]);
                break;
            case 'delete':
                $crud = DB::table('jenis_kendaraan')->where('id', $request->id)->update([
                    'deleted_at' => time()
                ]);
                break;
            default:
                break;
        }

        if (!$crud)
            return response()->json(['error' => 1, 'message' => "Data Gagal dieksekusi"], 400);

        return response()->json(['error' => 0, 'message' => "Data Berhasil di" . $request->type], 200);
    }

    public function datatable()
    {
        $jenis_kendaraan = JenisKendaraan::where('deleted_at', null)->orderBy('created_at', 'DESC')->get();
        return response()->json(['data' => $jenis_kendaraan], 200);
    }
}
