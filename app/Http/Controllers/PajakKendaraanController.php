<?php

namespace App\Http\Controllers;

use App\KTP;
use App\Kendaraan;
use App\PajakKendaraan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PajakKendaraanController extends Controller
{
    public function index()
    {
        $data = array(
            'title' => 'MANAGE PAJAK KENDARAAN',
            'subTitle' => 'Nomor Polisi : ',
            'id_kendaraan' => '',
            'special_css' => '',
            'special_js' => 'pajak_kendaraan.js'
        );

        return view('pajak_kendaraan', $data);
    }

    public function manage(Request $request)
    {
        switch ($request->type) {
            case 'insert':
                $crud = DB::table('pajak_kendaraan')->insert([
                    'id' => time(),
                    'bulan_pajak' => strtotime($request->bulan_pajak),
                    'biaya' => $request->biaya,
                    'no_va' => null,
                    'status' => null,
                    'id_kendaraan' => $request->id_kendaraan,
                    'created_at' => time()
                ]);
                break;
            case 'update':
                $crud = DB::table('pajak_kendaraan')->where('id', $request->id)->update([
                    'bulan_pajak' => strtotime($request->bulan_pajak),
                    'biaya' => $request->biaya,
                    'id_kendaraan' => $request->id_kendaraan,
                    'updated_at' => time()
                ]);
                break;
            case 'delete':
                $crud = DB::table('pajak_kendaraan')->where('id', $request->id)->update([
                    'deleted_at' => time()
                ]);
                break;
            case 'changeStatus':
            	if ($request->status == 1) {
	                $crud = DB::table('pajak_kendaraan')->where('id', $request->id)->update([
	                    'status' => 1,
	                    'updated_at' => time()
	                ]);
            	} else {
            		$crud = DB::table('pajak_kendaraan')->where('id', $request->id)->update([
	                    'no_va' => null,
                    	'status' => 2,
	                    'updated_at' => time()
	                ]);
            	}
            	
                break;
            default:
                break;
        }

        if (!$crud)
            return response()->json(['error' => 1, 'message' => "Data Gagal dieksekusi"], 400);

        return response()->json(['error' => 0, 'message' => "Data Berhasil di" . $request->type], 200);
    }

    public function datatable(Request $request)
    {
        $pajak_kendaraan = PajakKendaraan::where(['id_kendaraan' => $request->id_kendaraan, 'deleted_at' => null])->orderBy('created_at', 'DESC')->get();
        return response()->json(['data' => $pajak_kendaraan], 200);
    }

    public function getAllPajakKendaraanByKendaraan($id_kendaraan){

    	$kendaraan = Kendaraan::find($id_kendaraan);

    	$data = array(
            'title' => 'MANAGE PAJAK KENDARAAN',
            'subTitle' => 'Nomor Polisi : ' . $kendaraan->no_polisi,
            'id_kendaraan' => $id_kendaraan,
            'special_css' => '',
            'special_js' => 'pajak_kendaraan.js'
        );

        return view('pajak_kendaraan', $data);
    }
}
