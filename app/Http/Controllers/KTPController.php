<?php

namespace App\Http\Controllers;

use App\KTP;
use App\Provinsi;
use App\Kota;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KTPController extends Controller
{
    public function index()
    {
        $kota = Kota::with(['provinsi'])->get();   

        $data = array(
            'title' => 'MANAGE KTP',
            'special_css' => '',
            'special_js' => 'ktp.js',
            'kota' => $kota
        );

        return view('ktp', $data);
    }

    public function manage(Request $request)
    {
        switch ($request->type) {
            case 'insert':
                $crud = DB::table('ktp')->insert([
                    'id' => time(),
                    'id_kota' => $request->kota,
                    'nik' => $request->nik,
                    'nama' => ucwords($request->nama),
                    'tempat_lahir' => ucwords($request->tempat_lahir),
                    'tgl_lahir' => strtotime($request->tgl_lahir),
                    'jenis_kelamin' => ucwords($request->kelamin),
                    'golongan_darah' => ucwords($request->golongan),
                    'alamat' => ucwords($request->alamat),
                    'agama' => ucwords($request->agama),
                    'status_perkawinan' => ucwords($request->perkawinan),
                    'pekerjaan' => ucwords($request->pekerjaan),
                    'kewarganegaraan' => ucwords($request->kewarganegaraan),
                    'created_at' => time()
                ]);
                break;
            case 'update':
                $crud = DB::table('ktp')->where('id', $request->id)->update([
                    'id_kota' => $request->kota,
                    'nik' => $request->nik,
                    'nama' => ucwords($request->nama),
                    'tempat_lahir' => ucwords($request->tempat_lahir),
                    'tgl_lahir' => strtotime($request->tgl_lahir),
                    'jenis_kelamin' => ucwords($request->kelamin),
                    'golongan_darah' => ucwords($request->golongan),
                    'alamat' => ucwords($request->alamat),
                    'agama' => ucwords($request->agama),
                    'status_perkawinan' => ucwords($request->perkawinan),
                    'pekerjaan' => ucwords($request->pekerjaan),
                    'kewarganegaraan' => ucwords($request->kewarganegaraan),
                    'updated_at' => time()
                ]);
                break;
            case 'delete':
                $crud = DB::table('ktp')->where('id', $request->id)->update([
                    'deleted_at' => time()
                ]);
                break;
            case 'cek_ktp':
                if(!$request->nik){
                    return response()->json(['error' => 1, 'message' => "FORM NIK KTP HARUS TERISI"], 200);
                }

                $ktp = DB::table('ktp')->where('nik', $request->nik)->get()->toArray();

                if (count($ktp) > 0) {
                    $crud = DB::table('ktp')->where('nik', $request->nik)->get();
                    return response()->json(['error' => 0, 'message' => "NIK KTP SESUAI", 'ktp' => $crud], 200);
                } else {
                    return response()->json(['error' => 1, 'message' => "NIK KTP ANDA TIDAK ADA"], 200);
                }

                break;
            default:
                break;
        }

        if (!$crud)
            return response()->json(['error' => 1, 'message' => "Data Gagal dieksekusi"], 400);

        return response()->json(['error' => 0, 'message' => "Data Berhasil di" . $request->type], 200);
    }

    public function datatable()
    {
        $ktp = KTP::where('deleted_at', null)->orderBy('created_at', 'DESC')->get();
        return response()->json(['data' => $ktp], 200);
    }
}
