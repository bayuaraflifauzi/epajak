<?php

namespace App\Http\Controllers;

use App\KTP;
use App\Kendaraan;
use App\PajakKendaraan;
use App\JenisKendaraan;
use App\Histori;
use App\Kota;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function index($id_ktp)
    {
    	$ktp = KTP::find($id_ktp);
    	$jenis_kendaraan = JenisKendaraan::where(['deleted_at' => null])->orderBy('created_at', 'DESC')->get();
    	$kota = Kota::with(['provinsi'])->get();   

        $data = array(
            'title' => 'WAJIB PAJAK',
            'ktp' => $ktp,
            'jenis_kendaraan' => $jenis_kendaraan,
            'kota' => $kota,
            'special_css' => 'transaksi.css',
            'special_js' => 'transaksi.js'
        );

        return view('transaksi', $data);
    }

    public function manage(Request $request)
    {
        switch ($request->type) {
            case 'data-ktp':
                $ktp = KTP::find($request->id_ktp);
       			 return response()->json(['data' => $ktp], 200);
                break;
            default:
                break;
        }
    }

    public function bayarPajakDatatable(Request $request)
    {
    	$id_ktp = $request->id_ktp;

        $pajak_kendaraan = PajakKendaraan::with(['kendaraan', 'histori', 'kendaraan.jenisKendaraan'])
        ->whereHas('kendaraan', function($hasil) use($id_ktp) {
            $hasil->where('id_ktp', $id_ktp);
        })
        ->where(['status'=> null,'deleted_at' => null])
        ->orderBy('bulan_pajak', 'ASC')
        ->get();


        return response()->json(['data' => $pajak_kendaraan], 200);
    }

    public function historiKendaraanDatatable(Request $request)
    {
    	$histori_kendaraan = PajakKendaraan::with(['kendaraan', 'histori', 'kendaraan.jenisKendaraan'])
        ->where(['id_histori'=> $request->id_histori])
        ->orderBy('bulan_pajak', 'DESC')
        ->get();


        return response()->json(['data' => $histori_kendaraan], 200);
    }

    public function historiDatatable(Request $request)
    {
    	$id_ktp = $request->id_ktp;

        $histori = Histori::where(['deleted_at' => null])
        ->orderBy('created_at', 'DESC')
        ->get();


        return response()->json(['data' => $histori], 200);
    }

    public function generateVirtualAccount(Request $request)
    {
    	$list_kendaraan_pajak = $request->list_id_kendaraan;
    	$id_ktp = $request->id_ktp;
    	$total_bayar = $request->total;
    	$no_va = mt_rand(000000000000000, 999999999999999);
        $id_histori = time();

    	$crudHistori = DB::table('histori_pajak')->insert([
            'id' => $id_histori,
            'tanggal' => time(),
            'no_va' => $no_va,
            'total' => $total_bayar,
            'status' => 0,
            'id_ktp' => $id_ktp,
            'created_at' => time()
        ]);

        if (!$crudHistori)
            return response()->json(['error' => 1, 'message' => "Histori Gagal dieksekusi"], 400);

    	foreach ($list_kendaraan_pajak as $item) {
    		$crud = DB::table('pajak_kendaraan')->where('id', $item)->update([
                'no_va' => $no_va,
            	'status' => 0,
            	'id_histori' => $id_histori,
                'updated_at' => time()
            ]);
    	}

    	if (!$crud)
            return response()->json(['error' => 1, 'message' => "Bayar Pajak Gagal dieksekusi"], 400);

        return response()->json(['error' => 0, 'message' => "Data bayar pajak telah diperbaharui", 'va' => $no_va], 200);
        
    }
}
