<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KTP extends Model
{
    protected $table = 'ktp';
    protected $primaryKey = 'id';

    public $incrementing = false;
}
