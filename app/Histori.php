<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Histori extends Model
{
    protected $table = 'histori_pajak';
    protected $primaryKey = 'id';

    public $incrementing = false;

    public function ktp()
    {
        return $this->hasOne('App\KTP', 'id', 'id_ktp');
    }
}
