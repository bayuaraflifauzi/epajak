<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
 	protected $table = 'kota';
    protected $primaryKey = 'id';

    public $incrementing = false;
    public $timestamps = false;

    public function provinsi()
    {
        return $this->hasOne('App\Provinsi', 'id', 'id_provinsi');
    }
}
