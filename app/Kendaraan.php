<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kendaraan extends Model
{
    protected $table = 'kendaraan';
    protected $primaryKey = 'id';

    public $incrementing = false;

    public function jenisKendaraan()
    {
        return $this->hasOne('App\JenisKendaraan', 'id', 'id_jenis_kendaraan');
    }
}
