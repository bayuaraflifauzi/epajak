<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PajakKendaraan extends Model
{
    protected $table = 'pajak_kendaraan';
    protected $primaryKey = 'id';

    public $incrementing = false;

    public function kendaraan()
    {
        return $this->hasOne('App\Kendaraan', 'id', 'id_kendaraan');
    }

    public function histori()
    {
        return $this->hasOne('App\Histori', 'id', 'id_histori');
    }
}
