@extends('master.master')
@section('library-css')
@if ($special_css)
<link href="{{ asset("app/css/".$special_css) }}" rel="stylesheet">
@endif
@endsection
@section('library-js')
@if ($special_js)
<script src="{{ asset("app/js/".$special_js) }}"></script>
@endif
@endsection
@section('content')
<div class="row justify-content-md-center">
   <div class="col-md-10">
      <h3 class="text-center " style="color: #EEF1F4;">{{ $title }}</h3>
      <h1 class="text-center text-uppercase mb-0" style="color: #EEF1F4;"><strong>{{ $ktp->nama }}</strong></h1>
      <hr style="border: 1px solid #EEF1F4; width: 50%" class="mt-0 mb-1">
      <h3 class="text-center" style="color: #EEF1F4;">{{ $ktp->nik }}</h3>
   </div>
</div>
<div class="container">
   <div class="card">
      <div class="card-header" hidden>
         <div class="input-group" style="width: 30%; float:right;">
            <input type="text" class="form-control" name="id_ktp" id="id-ktp-form" value="{{ $ktp->id }}" hidden>
            <input type="text" class="form-control" placeholder="Bulan" aria-label="bulan" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <button class="btn btn-primary" type="button">Cari</button>
            </div>
         </div>
      </div>
      <div class="card-body">
         <ul class="nav nav-tabs mb-4 nav-pills" id="myTab" role="tablist">
            <li class="nav-item font-weight-bold pill-1">
               <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><strong>Kendaraan Bayar Pajak</strong></a>
            </li>
            <li class="nav-item font-weight-bold pill-2">
               <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Info KTP Wajib Pajak</a>
            </li>
            <li class="nav-item font-weight-bold pill-3">
               <a class="nav-link" id="kendaraan-tab" data-toggle="tab" href="#kendaraan" role="tab" aria-controls="kendaraan" aria-selected="false">Kendaraan Wajib Pajak</a>
            </li>
            <li class="nav-item font-weight-bold pill-4">
               <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">History Bayar Pajak</a>
            </li>
         </ul>
         <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
               <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="table-bayar-pajak">
                     <thead>
                        <tr>
                           <th class="text-center">No</th>
                           <th class="text-center">Bulan Pajak</th>
                           <th class="text-center">Merk</th>
                           <th class="text-center">Model</th>
                           <th class="text-center">No Polisi</th>
                           <th class="text-center">Biaya Pajak</th>
                           <th class="text-center">Aksi</th>
                           <th class="text-center">Pilih</th>
                        </tr>
                     </thead>
                     <tbody></tbody>
                  </table>
                  <button type="button" class="btn btn-primary mt-3 w-100" onclick="reviewSelected()">Lanjut Daftar Online</button>
               </div>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
               <form id="form-ktp">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Kota/Kabupaten</label>
                           <select class="form-control" name="kota" id="kota" disabled>
                              <option value="">==== pilih kota/kabupaten ====</option>
                              @foreach($kota as $value)
                              <option value="{{ $value->id }}">{{ $value->provinsi->name }} - {{ $value->name }}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="form-group">
                           <label class="control-label font-weight-bold">NIK</label>
                           <input class="form-control" type="text" placeholder="Masukkan NIK" name="nik" disabled>
                        </div>
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Nama Lengkap</label>
                           <input class="form-control" type="text" placeholder="Masukkan nama lengkap" name="nama" disabled>
                        </div>
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Tempat Lahir</label>
                           <input class="form-control" type="text" placeholder="Masukkan tampat lahir" name="tempat_lahir" disabled>
                        </div>
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Tanggal Lahir</label>
                           <input class="form-control" type="text" placeholder="Masukkan tanggal lahir" name="tgl_lahir" id="tgl-lahir" disabled>
                        </div>
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Jenis Kelamin</label>
                           <select class="form-control" name="kelamin" id="kelamin" disabled>
                              <option value="">==== pilih jenis kelamin ====</option>
                              <option value="L">Laki-Laki</option>
                              <option value="P">Perempuan</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Golongan Darah</label>
                           <select class="form-control" name="golongan" id="golongan" disabled>
                              <option value="">==== pilih golongan darah ====</option>
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="AB">AB</option>
                              <option value="O">O</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Alamat</label>
                           <textarea class="form-control" id="alamat" name="alamat" placeholder="Masukkan alamat lengkap" rows="5" disabled></textarea>
                        </div>
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Agama</label>
                           <select class="form-control" name="agama" id="agama" disabled>
                              <option value="">==== pilih agama ====</option>
                              <option value="ISLAM">ISLAM</option>
                              <option value="PROTESTAN">PROTESTAN</option>
                              <option value="KATOLIK">KATOLIK</option>
                              <option value="HINDU">HINDU</option>
                              <option value="BUDDHA">BUDDHA</option>
                              <option value="KONGHUCU">KONGHUCU</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Status Perkawinan</label>
                           <select class="form-control" name="perkawinan" id="perkawinan" disabled>
                              <option value="">==== pilih status perkawinan ====</option>
                              <option value="BELUM_KAWIN">BELUM KAWIN</option>
                              <option value="KAWIN">KAWIN</option>
                              <option value="CERAI_HIDUP">CERAI HIDUP</option>
                              <option value="CERAI_MATI">CERAI MATI</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Pekerjaan</label>
                           <input class="form-control" type="text" placeholder="Masukkan pekerjaan" name="pekerjaan" disabled>
                        </div>
                        <div class="form-group">
                           <label class="control-label font-weight-bold">Kewarganegaraan</label>
                           <select class="form-control" name="kewarganegaraan" id="kewarganegaraan" disabled>
                              <option value="">==== pilih kewarganegaraan ====</option>
                              <option value="WNI">WNI (Warga Negara Indonesia)</option>
                              <option value="WNA">WNA (Warga Negara Asing)</option>
                           </select>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
            <div class="tab-pane fade" id="kendaraan" role="tabpanel" aria-labelledby="kendaraan-tab">
               <div class="table-responsive">
                  <table class="table table-hover table-bordered w-100" id="table-kendaraan-wb">
                     <thead>
                        <tr>
                           <th class="text-center">No</th>
                           <th class="text-center">Merk</th>
                           <th class="text-center">No Polisi</th>
                           <th class="text-center">No Rangka</th>
                           <th class="text-center">No Mesin</th>
                           <th class="text-center">Aksi</th>
                        </tr>
                     </thead>
                     <tbody></tbody>
                  </table>
               </div>
            </div>
            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
               <div class="table-responsive">
                  <table class="table table-hover table-bordered w-100" id="table-histori">
                     <thead>
                        <tr>
                           <th class="text-center">No</th>
                           <th class="text-center">Tanggal Transaksi</th>
                           <th class="text-center">No Virtual Account</th>
                           <th class="text-center">Total Biaya Pajak</th>
                           <th class="text-center">Status</th>
                           <th class="text-center">Aksi</th>
                        </tr>
                     </thead>
                     <tbody></tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <div class="card-footer">
         <button type="button" class="btn btn-secondary" onclick="window.location.href = window.base_url;">Kembali</button>
      </div>
   </div>
</div>
<div id="form-modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="form">
            {{ csrf_field() }}
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group" hidden>
                        <label class="control-label">ID</label>
                        <input class="form-control" type="text" name="id">
                     </div>
                     <div class="form-group" hidden>
                        <label class="control-label">Type</label>
                        <input class="form-control" type="text" name="type">
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Jenis Kendaraan</label>
                        <select class="form-control" name="id_jenis_kendaraan" id="jenis-kendaraan" disabled>
                           <option value="">==== pilih jenis kendaraan ====</option>
                           @foreach($jenis_kendaraan as $value)
                           <option value="{{ $value->id }}">{{ $value->nama }}</option>
                           @endforeach
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Merk</label>
                        <input class="form-control" type="text" placeholder="Masukkan NIK" name="merk" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Tahun Kendaraan</label>
                        <input class="form-control" type="text" placeholder="Masukkan tahun kendaraan" name="tahun_kendaraan" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Model</label>
                        <input class="form-control" type="text" placeholder="Masukkan model" name="model" disabled>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Warna</label>
                        <input class="form-control" type="text" placeholder="Masukkan warna" name="warna" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Nomor Rangka</label>
                        <input class="form-control" type="text" placeholder="Masukkan nomor rangka" name="no_rangka" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Nomor Mesin</label>
                        <input class="form-control" type="text" placeholder="Masukkan nomor mesin" name="no_mesin" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Nomor Polisi</label>
                        <input class="form-control" type="text" placeholder="Masukkan nomor polisi" name="no_polisi" disabled>
                     </div>
                     <div class="form-group" hidden>
                        <label class="control-label font-weight-bold">ID KTP</label>
                        <input class="form-control" type="text" placeholder="Masukkan ID KTP" name="id_ktp" disabled>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Bulan Pajak</label>
                        <input class="form-control" type="text" placeholder="Masukkan bulan pajak" data-provide="datepicker" name="bulan_pajak" id="bulan-pajak" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Biaya Pajak</label>
                        <input class="form-control" type="text" placeholder="Masukkan biaya pajak" name="biaya" disabled>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div id="form-total-pajak" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header" style="text-align: center; display: inline;">
            <h5 id="label-judul"></h5>
         </div>
         <form>
            <div class="modal-body text-center">
               <h1 id="label-total" class="font-weight-bold"></h1>
               <h3>Apakah anda yakin akan memproses bayar pajak kendaraan selanjutnya?</h3>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-success" onclick="generate()">Proses Bayar</button>
               <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div id="form-va" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header" style="text-align: center; display: inline;">
            <h5 id="label-judul-va"></h5>
         </div>
         <form>
            <div class="modal-body text-center">
               <h1 id="label-va" class="font-weight-bold"></h1>
               <h3>KETERANGAN</h3>
               <h4>Virtual Account ini digunakan untuk membayar pakjak kendaraan Wajib Pajak melalui fasilitas bank BJB. Screenshoot dan simpan halamain ini. Terimakasih.</h4>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary w-100" data-dismiss="modal">Tutup</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div id="table-modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title label-table"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
        <div class="modal-body">
          <div class="table-responsive">
              <table class="table table-hover table-bordered w-100" id="table-histori-kendaraan">
                 <thead>
                    <tr>
                       <th class="text-center">No</th>
                       <th class="text-center">Bulan Pajak</th>
                       <th class="text-center">Merk</th>
                       <th class="text-center">Model</th>
                       <th class="text-center">No Polisi</th>
                       <th class="text-center">Biaya Pajak</th>
                    </tr>
                 </thead>
                 <tbody></tbody>
              </table>
           </div>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        </div>
      </div>
   </div>
</div>
<div id="form-modal-kendaraan" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="form-kendaraan">
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Jenis Kendaraan</label>
                        <select class="form-control" name="id_jenis_kendaraan" id="jenis-kendaraan" disabled>
                           <option value="">==== pilih jenis kendaraan ====</option>
                           @foreach($jenis_kendaraan as $value)
                           <option value="{{ $value->id }}">{{ $value->nama }}</option>
                           @endforeach
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Merk</label>
                        <input class="form-control" type="text" placeholder="Masukkan NIK" name="merk" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Tahun Kendaraan</label>
                        <input class="form-control" type="text" placeholder="Masukkan tahun kendaraan" name="tahun_kendaraan" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Model</label>
                        <input class="form-control" type="text" placeholder="Masukkan model" name="model" disabled>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Warna</label>
                        <input class="form-control" type="text" placeholder="Masukkan warna" name="warna" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Nomor Rangka</label>
                        <input class="form-control" type="text" placeholder="Masukkan nomor rangka" name="no_rangka" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Nomor Mesin</label>
                        <input class="form-control" type="text" placeholder="Masukkan nomor mesin" name="no_mesin" disabled>
                     </div>
                     <div class="form-group">
                        <label class="control-label font-weight-bold">Nomor Polisi</label>
                        <input class="form-control" type="text" placeholder="Masukkan nomor polisi" name="no_polisi" disabled>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
            </div>
         </form>
      </div>
   </div>
</div>
@endsection