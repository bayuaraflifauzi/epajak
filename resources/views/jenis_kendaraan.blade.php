@extends('master.master')
@section('library-css')
@if ($special_css)
<link href="{{ asset("app/css/".$special_css) }}" rel="stylesheet">
@endif
@endsection
@section('library-js')
@if ($special_js)
<script src="{{ asset("app/js/".$special_js) }}"></script>
@endif
@endsection
@section('content')
<div class="row justify-content-md-center">
   <div class="col-md-10">
      <h1 class="text-center" style="color: #EEF1F4;"><strong>{{ $title }}</strong></h1>
      <hr style="border: 1px solid #EEF1F4;" class="mt-0">
   </div>
</div>
<div class="row justify-content-md-center">
   <div class="col-md-10">
      <div class="card">
         <div class="card-header">
            <button type="button" onclick="openForm()" class="btn btn-primary">Tambah</button>
         </div>
         <div class="card-body">
            <div class="table-responsive">
               <table class="table table-hover table-bordered" id="table-jenis-kendaraan">
                  <thead>
                     <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Jenis Kendaraan</th>
                        <th class="text-center">Aksi</th>
                     </tr>
                  </thead>
                  <tbody></tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="form-modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="form">
            {{ csrf_field() }}
            <div class="modal-body">
               <div class="form-group" hidden>
                  <label class="control-label">ID</label>
                  <input class="form-control" type="text" name="id">
               </div>
               <div class="form-group" hidden>
                  <label class="control-label">Type</label>
                  <input class="form-control" type="text" name="type">
               </div>
               <div class="form-group">
                  <label class="control-label">Jenis Kendaraan</label>
                  <input class="form-control" type="text" placeholder="Masukkan nama jenis kendaraan" name="nama" required>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" id="submit" class="btn btn-primary">Simpan</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
         </form>
      </div>
   </div>
</div>
@endsection