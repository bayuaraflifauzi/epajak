<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>E-Pajak Kendaraan Bermotor</title>
      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset("template/bootstrap/css/bootstrap.css") }}">
      <link rel="stylesheet" href="{{ asset("template/plugins/jquery.dataTables.css") }}">
      <link rel="stylesheet" href="{{ asset("template/plugins/sweetalert2.min.css") }}">
      <link rel="stylesheet" href="{{ asset("template/plugins/bootstrap-datepicker.min.css") }}">
      <link href="{{ asset("template/plugins/select2/css/select2.min.css") }}" rel="stylesheet">
      <link href="{{ asset("template/plugins/select2/css/select2-bootstrap.min.css") }}" rel="stylesheet">
      <!-- Styles -->
      <style>
         html, body {
         background-color: #0167C5;
         font-family: 'Nunito', sans-serif;
         font-weight: 200;
         }
      </style>
      @yield('library-css')
   </head>
   <style type="text/css">
      #overlay {
      z-index: 999999;
      display: none;
      position: fixed;
      top: 0px;
      left: 0px;
      width: 100%;
      height: 100%;
      background: rgba(4, 10, 30, 0.8);
      }
      #tengah {
      width: 250px;
      height: 30px;
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      margin: auto;
      }
      #name-web-admin {
      display: none;
      }
      #sub-name-web-admin {
      display: inline;
      }
      @media screen and (min-width: 768px) {
      #name-web-admin {
      display: inline;
      }
      #sub-name-web-admin {
      display: none;
      }
      }
   </style>
   <div id="overlay">
      <div id="tengah">
         <center>
            <br>
            <span style="color:#ffffff">Loading</span><br>
            <img src="{{ asset("loading.gif") }}">
         </center>
      </div>
   </div>
   <body>
      <div class="container mt-4 mb-4">
         @yield('content')
      </div>
      <script type="text/javascript" src="{{ asset("template/js/jquery-3.5.1.js") }}"></script>
      <script type="text/javascript" src="{{ asset("template/js/popper.min.js") }}"></script>
      <script type="text/javascript" src="{{ asset("template/bootstrap/js/bootstrap.bundle.js") }}"></script>
      <script type="text/javascript" src="{{ asset("template/plugins/jQuery-confirm/jquery-confirm.min.js") }}"></script>
      <script type="text/javascript" src="{{ asset("template/plugins/jQuery/jquery-validate.js") }}"></script>
      <!-- Data table plugin-->
      <script type="text/javascript" src="{{ asset("template/plugins/jquery.dataTables.js") }}"></script>
      <!-- Sweetalert-->
      <script type="text/javascript" src="{{ asset("template/plugins/sweetalert2.all.min.js") }}"></script>
      <script type="text/javascript" src="{{ asset("template/plugins/sweetalert2.min.js") }}"></script>

      <script type="text/javascript" src="{{ asset("template/plugins/bootstrap-datepicker.min.js") }}"></script>
      <script type="text/javascript" src="{{ asset("template/plugins/select2/js/select2.min.js") }}"></script>
      
      <script type="text/javascript">
        window.base_url = window.location.protocol + "//" + window.location.host;

         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         
         $.validator.setDefaults({
             errorElement: 'em',
             onfocusout: function (element) {
                 $(element).valid();
             },
             errorPlacement: function (error, element) {
                 error.addClass('invalid-feedback');
                 if (element.prop('type') === 'checkbox') {
                     error.insertAfter(element.parent('label'));
                 } else {
                     error.insertAfter(element);
                 }
             },
             highlight: function highlight(element) {
                 $(element).addClass('is-invalid').removeClass('is-valid');
             },
             unhighlight: function unhighlight(element) {
                 $(element).addClass('is-valid').removeClass('is-invalid');
             },
             submitHandler: function (form) {
                 const data = $("#form").serialize();
                 submitData(data, function (status, data) {
                     if (status && typeof formCallback === "function") {
                         formCallback(data);
                     }
                 });
                 return false;
             }
         });
         
         function submitData(formData, callback = null) {
             $.ajax({
                 url: window.base_url + "/" + manage,
                 type: "POST",
                 data: formData,
                 dataType: "JSON",
                 beforeSend: function () {
                     $("#overlay").css("display", "block");
                 },
                 success: function (data) {
                     successAction(data);
                 },
                 error: function (jqXHR, textStatus, errorThrown) {
                     $("#overlay").css("display", "none");
                     sweetalert(1, "Proses dihentikan periksa form yang Anda isi");
                     form.find("button[type=submit]").removeAttr("disabled");
                     if (callback != null) {
                         callback(false, textStatus);
                     }
                 }
             })
         }
         
         $("a.card-header-action").on("click", function (e) {
             e.preventDefault();
             const type = $(this).data("type");
         
             switch (type) {
                 case "refresh-datatable":
                     refreshData();
                     break;
                 case "add-data":
                     openForm();
                     break;
                 case "export-import-datatable":
                     exportImportData();
                     break;
                 case "filter-datatable":
                     openFilterForm();
                     break;
                 default:
                     break;
             }
         });
         
         $(".form-check").click(function () {
             var check = $(this).find("input[type='checkbox']:checked").length;
             if (check > 0) {
                 $(this).parent().parent().find("select").attr("disabled", false);
                 $(this).parent().parent().find("input[type='text']").attr("disabled", false);
                 $(this).parent().parent().find("input[type='radio']").attr("disabled", false);
             } else {
                 $(this).parent().parent().find("select").attr("disabled", true);
                 $(this).parent().parent().find("input[type='text']").attr("disabled", true);
                 $(this).parent().parent().find("input[type='radio']").attr("disabled", true);
                 $(this).parent().parent().find("input[type='text']").val("");
                 $(this).parent().parent().find("select").val("").trigger('change');
             }
         })
         
         function resetForm($modal) {
             window.validation.resetForm();
             $($modal).on('hidden.bs.modal', function () {
                 $(this).find('form')[0].reset();
                 $(this).find('select').val("").trigger('change');
                 $(this).find("input").removeClass("is-valid");
                 $(this).find("input").removeClass("is-invalid");
                 $(this).find("select").removeClass("is-valid");
                 $(this).find("select").removeClass("is-invalid");
                 $(this).find("textarea").removeClass("is-valid");
                 $(this).find("textarea").removeClass("is-invalid");
             });
         }
         
         function convertMilisecondToDate(time) {
            var date = new Date(time * 1000);
            var dd = date.getDate();
            var mm = date.getMonth() + 1;
            var yyyy = date.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            return yyyy + '-' + mm;
        }

        function convertMilisecondToDateView(time) {
            var date = new Date(time * 1000);
            var dd = date.getDate();
            var mm = date.getMonth() + 1;
            var yyyy = date.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            return yyyy + '-' + mm + '-' + dd;
        }

         function sweetalert(error, message) {
             var title = ["Sukses!", "Info!", "Gagal!"];
             var type = ["success", "info", "error"];
             Swal.fire({
                 icon: type[error],
                 title: title[error],
                 text: message,
                 showConfirmButton: false,
                 timer: 1500
               })
         }
         
         function refreshData() {
             try {
                 table.ajax.reload();
                 $("i.fa-refresh").addClass("fa-spin");
             } catch (err) {}
         }
         
         function closeForm() {
             $("#form-modal").modal("hide");
         }
         
         function removeData(url, object) {
             Swal.fire({
                 title: 'Apakah kamu yakin?',
                 text: "Data yang dipilih akan dihapus!",
                 icon: 'warning',
                 showCancelButton: true,
                 confirmButtonColor: '#3085d6',
                 cancelButtonColor: '#d33',
                 confirmButtonText: 'Yes, delete it!'
               }).then((result) => {
                  if (result.value) {
                      $.ajax({
                                 url: url,
                                 type: "POST",
                                 data: {
                                     id: object.id,
                                     type: 'delete'
                                 },
                                 dataType: "JSON",
                                 beforeSend: function () {
                                     $("#overlay").css("display", "block");
                                 },
                                 success: function (row) {
                                     $("#overlay").css("display", "none");
                                     sweetalert(row.error, row.message);
                                     refreshData();
                                 }
                             });
                    }
                 });
         }

         function formatRupiah(angka, prefix){
            var number_string = angka.toString().replace(/[^,\d]/g, ''),
            split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiah          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
 
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
 
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
      </script>
      @yield('library-js')
   </body>
</html>