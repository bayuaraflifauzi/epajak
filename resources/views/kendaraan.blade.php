@extends('master.master')
@section('library-css')
@if ($special_css)
<link href="{{ asset("app/css/".$special_css) }}" rel="stylesheet">
@endif
@endsection
@section('library-js')
@if ($special_js)
<script src="{{ asset("app/js/".$special_js) }}"></script>
@endif
@endsection
@section('content')
<h1 class="text-center" style="color: #EEF1F4;"><strong>{{ $title }}</strong></h1>
<hr style="border: 1px solid #EEF1F4;" class="mt-0">
<h2 class="text-center" style="color: #EEF1F4">{{ $subTitle }}</h2>
<input class="form-control" type="text" name="id_ktp_now" id="id-ktp-now" value="{{ $id_ktp }}" hidden>
<div class="card">
   <div class="card-header">
      <button type="button" onclick="openForm()" class="btn btn-primary">Tambah</button>
   </div>
   <div class="card-body">
      <div class="table-responsive">
         <table class="table table-hover table-bordered" id="table-kendaraan">
            <thead>
               <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Merk</th>
                  <th class="text-center">No Polisi</th>
                  <th class="text-center">No Rangka</th>
                  <th class="text-center">No Mesin</th>
                  <th class="text-center">Aksi</th>
               </tr>
            </thead>
            <tbody></tbody>
         </table>
      </div>
   </div>
   <div class="card-footer">
      <button type="button" onclick="window.history.back()" class="btn btn-danger">Kembali</button>
   </div>
</div>

<div id="form-modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="form">
            {{ csrf_field() }}
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group" hidden>
                        <label class="control-label">ID</label>
                        <input class="form-control" type="text" name="id">
                     </div>
                     <div class="form-group" hidden>
                        <label class="control-label">Type</label>
                        <input class="form-control" type="text" name="type">
                     </div>
                     <div class="form-group">
                        <label class="control-label">Jenis Kendaraan</label>
                        <select class="form-control" name="id_jenis_kendaraan" id="jenis-kendaraan" required>
                           <option value="">==== pilih jenis kendaraan ====</option>
                           @foreach($jenis_kendaraan as $value)
                           <option value="{{ $value->id }}">{{ $value->nama }}</option>
                           @endforeach
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Merk</label>
                        <input class="form-control" type="text" placeholder="Masukkan NIK" name="merk" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Tahun Kendaraan</label>
                        <input class="form-control" type="text" placeholder="Masukkan tahun kendaraan" name="tahun_kendaraan" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Model</label>
                        <input class="form-control" type="text" placeholder="Masukkan model" name="model" required>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Warna</label>
                        <input class="form-control" type="text" placeholder="Masukkan warna" name="warna" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Nomor Rangka</label>
                        <input class="form-control" type="text" placeholder="Masukkan nomor rangka" name="no_rangka" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Nomor Mesin</label>
                        <input class="form-control" type="text" placeholder="Masukkan nomor mesin" name="no_mesin" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Nomor Polisi</label>
                        <input class="form-control" type="text" placeholder="Masukkan nomor polisi" name="no_polisi" required>
                     </div>
                     <div class="form-group" hidden>
                        <label class="control-label">ID KTP</label>
                        <input class="form-control" type="text" placeholder="Masukkan ID KTP" name="id_ktp" value="{{ $id_ktp }}" required>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" id="submit" class="btn btn-primary">Simpan</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
         </form>
      </div>
   </div>
</div>
@endsection