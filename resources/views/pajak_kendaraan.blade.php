@extends('master.master')
@section('library-css')
@if ($special_css)
<link href="{{ asset("app/css/".$special_css) }}" rel="stylesheet">
@endif
@endsection
@section('library-js')
@if ($special_js)
<script src="{{ asset("app/js/".$special_js) }}"></script>
@endif
@endsection
@section('content')
<h1 class="text-center" style="color: #EEF1F4;"><strong>{{ $title }}</strong></h1>
<hr style="border: 1px solid #EEF1F4;" class="mt-0">
<h2 class="text-center" style="color: #EEF1F4">{{ $subTitle }}</h2>
<input class="form-control" type="text" name="id_kendaraan_now" id="id-kendaraan-now" value="{{ $id_kendaraan }}" hidden>
<div class="card">
   <div class="card-header">
      <button type="button" onclick="openForm()" class="btn btn-primary">Tambah</button>
   </div>
   <div class="card-body">
      <div class="table-responsive">
         <table class="table table-hover table-bordered" id="table-pajak-kendaraan">
            <thead>
               <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Bulan Pajak</th>
                  <th class="text-center">Biaya Pajak</th>
                  <th class="text-center">Aksi</th>
               </tr>
            </thead>
            <tbody></tbody>
         </table>
      </div>
   </div>
   <div class="card-footer">
      <button type="button" onclick="window.history.back()" class="btn btn-danger">Kembali</button>
   </div>
</div>
<div id="form-modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="form">
            {{ csrf_field() }}
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group" hidden>
                        <label class="control-label">ID</label>
                        <input class="form-control" type="text" name="id">
                     </div>
                     <div class="form-group" hidden>
                        <label class="control-label">Type</label>
                        <input class="form-control" type="text" name="type">
                     </div>
                     <div class="form-group">
                        <label class="control-label">Bulan Pajak</label>
                        <input class="form-control" type="text" placeholder="Masukkan bulan pajak" data-provide="datepicker" name="bulan_pajak" id="bulan-pajak" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Biaya Pajak</label>
                        <input class="form-control" type="text" placeholder="Masukkan biaya pajak" name="biaya" required>
                     </div>
                     <div class="form-group" hidden>
                        <label class="control-label">ID Kendaraan</label>
                        <input class="form-control" type="text" placeholder="Masukkan ID Kendaraan" name="id_kendaraan" value="{{ $id_kendaraan }}" required>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" id="submit" class="btn btn-primary">Simpan</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-status" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Edit Status Pembayaran Pajak</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group" hidden>
               <label class="control-label">ID</label>
               <input class="form-control" type="text" name="id">
            </div>
            <div class="form-group">
               <label class="control-label">Bulan Pajak</label>
               <input class="form-control" type="text" placeholder="Masukkan bulan pajak" data-provide="datepicker" name="bulan" id="bulan"disabled>
            </div>
            <div class="form-group">
               <label class="control-label">Biaya Pajak</label>
               <input class="form-control" type="text" placeholder="Masukkan biaya pajak" name="biaya" disabled>
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
            <button type="button" class="btn btn-danger" id="button-batal" onclick="changeStatus(0)" hidden>Batalkan Pembayaran</button>
            <button type="button" class="btn btn-success" id="button-terima" onclick="changeStatus(1)" hidden>Terima Pebayaran</button>
         </div>
      </div>
   </div>
</div>
@endsection