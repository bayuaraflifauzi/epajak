@extends('master.master')
@section('library-css')
@if ($special_css)
<link href="{{ asset("app/css/".$special_css) }}" rel="stylesheet">
@endif
@endsection
@section('library-js')
@if ($special_js)
<script src="{{ asset("app/js/".$special_js) }}"></script>
@endif
@endsection
@section('content')
<img src="{{ asset("logo-samsat.png") }}" height="180px" width="210px" class="rounded mx-auto d-block mb-2" alt="Responsive image">
<h1 class="text-center mb-2" style="color: #EEF1F4;"><strong>SISTEM INFORMASI<BR>PEMBAYARAN PAJAK KENDARAAN BERMOTOR</strong></h1>
<div class="row justify-content-md-center mb-4">
   <div class="col-md-8">
      <form id="form">
         {{ csrf_field() }}
         <div class="form-group" hidden>
            <label class="control-label">Type</label>
            <input class="form-control" type="text" name="type" value="cek_ktp">
         </div>
         <div class="form-group">
            <input type="text" class="form-control" name="nik" id="nik" placeholder="Masukkan nomor induk kependudukan">
         </div>
         <button type="submit" id="submit" class="btn btn-primary" style="width: 100%; background-color: #0B2E9A; border-color: #0B2E9A">CARI</button>
      </form>
   </div>
</div>
<h2 class="text-center mb-4" style="color: #EEF1F4;"><strong>Bekerja Sama Dengan</strong></h2>
<div class="row justify-content-md-center">
   <div class="col-md-5">
      <img src="{{ asset("disdukcapil.png") }}" height="100px" width="350px" class="rounded mx-auto d-block" alt="Responsive image">
   </div>
   <div class="col-md-5">
      <img src="{{ asset("bjb.jpg") }}" height="100px" width="250px" class="rounded mx-auto d-block" alt="Responsive image">
   </div>
</div>
@endsection