@extends('master.master')
@section('library-css')
@if ($special_css)
<link href="{{ asset("app/css/".$special_css) }}" rel="stylesheet">
@endif
@endsection
@section('library-js')
@if ($special_js)
<script src="{{ asset("app/js/".$special_js) }}"></script>
@endif
@endsection
@section('content')
<h1 class="text-center" style="color: #EEF1F4;"><strong>{{ $title }}</strong></h1>
<hr style="border: 1px solid #EEF1F4;" class="mt-0">
<div class="card">
   <div class="card-header">
      <button type="button" onclick="openForm()" class="btn btn-primary">Tambah</button>
   </div>
   <div class="card-body">
      <div class="table-responsive">
         <table class="table table-hover table-bordered" id="table-ktp">
            <thead>
               <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">NIK</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Alamat</th>
                  <th class="text-center">Aksi</th>
               </tr>
            </thead>
            <tbody></tbody>
         </table>
      </div>
   </div>
</div>
<div id="form-modal" class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="form">
            {{ csrf_field() }}
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group" hidden>
                        <label class="control-label">ID</label>
                        <input class="form-control" type="text" name="id">
                     </div>
                     <div class="form-group" hidden>
                        <label class="control-label">Type</label>
                        <input class="form-control" type="text" name="type">
                     </div>
                     <div class="form-group">
                        <label class="control-label">Kota/Kabupaten</label>
                        <select class="form-control" name="kota" id="kota" required>
                           <option value="">==== pilih kota/kabupaten ====</option>
                           @foreach($kota as $value)
                           <option value="{{ $value->id }}">{{ $value->provinsi->name }} - {{ $value->name }}</option>
                           @endforeach
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label">NIK</label>
                        <input class="form-control" type="text" placeholder="Masukkan NIK" name="nik" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Nama Lengkap</label>
                        <input class="form-control" type="text" placeholder="Masukkan nama lengkap" name="nama" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Tempat Lahir</label>
                        <input class="form-control" type="text" placeholder="Masukkan tampat lahir" name="tempat_lahir" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Tanggal Lahir</label>
                        <input class="form-control" type="text" placeholder="Masukkan tanggal lahir" name="tgl_lahir" id="tgl-lahir" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Jenis Kelamin</label>
                        <select class="form-control" name="kelamin" id="kelamin" required>
                           <option value="">==== pilih jenis kelamin ====</option>
                           <option value="L">Laki-Laki</option>
                           <option value="P">Perempuan</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Golongan Darah</label>
                        <select class="form-control" name="golongan" id="golongan" required>
                           <option value="">==== pilih golongan darah ====</option>
                           <option value="A">A</option>
                           <option value="B">B</option>
                           <option value="AB">AB</option>
                           <option value="O">O</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Alamat</label>
                        <textarea class="form-control" id="alamat" name="alamat" placeholder="Masukkan alamat lengkap" rows="5" required></textarea>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Agama</label>
                        <select class="form-control" name="agama" id="agama" required>
                           <option value="">==== pilih agama ====</option>
                           <option value="ISLAM">ISLAM</option>
                           <option value="PROTESTAN">PROTESTAN</option>
                           <option value="KATOLIK">KATOLIK</option>
                           <option value="HINDU">HINDU</option>
                           <option value="BUDDHA">BUDDHA</option>
                           <option value="KONGHUCU">KONGHUCU</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Status Perkawinan</label>
                        <select class="form-control" name="perkawinan" id="perkawinan" required>
                           <option value="">==== pilih status perkawinan ====</option>
                           <option value="BELUM_KAWIN">BELUM KAWIN</option>
                           <option value="KAWIN">KAWIN</option>
                           <option value="CERAI_HIDUP">CERAI HIDUP</option>
                           <option value="CERAI_MATI">CERAI MATI</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Pekerjaan</label>
                        <input class="form-control" type="text" placeholder="Masukkan pekerjaan" name="pekerjaan" required>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Kewarganegaraan</label>
                        <select class="form-control" name="kewarganegaraan" id="kewarganegaraan" required>
                           <option value="">==== pilih kewarganegaraan ====</option>
                           <option value="WNI">WNI (Warga Negara Indonesia)</option>
                           <option value="WNA">WNA (Warga Negara Asing)</option>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" id="submit" class="btn btn-primary">Simpan</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
         </form>
      </div>
   </div>
</div>
@endsection